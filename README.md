# Der-Eddys Dota2 Autoexec Scripts

Als Zip Archiv downloaden und in  
```
SteamFolder\SteamApps\common\dota 2 beta\dota\cfg
```


kopieren, bei den meisten ist der Pfad wahrscheinlich:

```
C:\Program Files (x86)\Steam\SteamApps\common\dota 2 beta\dota\cfg
```


# Wenn man nachschauen möchte ob die Dateien geladen sind:  
Dafür schaut man in der Source Konsole nach  
Die Source Konsole für Dota2 bekommt man entweder über den Startparameter
```
-console
```
welcher die Konsole bei jedem Start öffnet, oder:

```
+con_enable 1
```
welche sie nur per Tastenbefehl (Standard bei deutschen Tastaturen ist "´") öffnet